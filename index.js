const http = require('http');   //importation du module http pour la création d'un serveur

const app = require('./app');   // importation de l'application express

const server = http.createServer(app);  //création du server 

const port = 8000;

server.listen(port, ()=>{
    console.log('le serveur marche sur le port: ', port);
});



