
const mongoose = require('mongoose');

// création du schema expérience
const experienceSchema = mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true}
});

// création du schema employee
const employeesSchema = mongoose.Schema({
    nom: {type: String, required: true},
    prenom: {type: String, required: true},
    age: {type: Number, required: true},
    poste: {type: String, required: true},
    experience: {type: experienceSchema, required: true}
});

module.exports = mongoose.model('employee', employeesSchema);