const express = require("express");
const employeesRoutes = require("./routes/employees"); // importation des routes
const mongoose = require("mongoose");

mongoose
  .connect("mongodb://localhost:27017/Employees")
  .then(() => console.log("base de donnée connectée"))
  .catch((err) => console.log(err));

const app = express();

// modification des headers pour éviter les problèmes cors
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*'); 
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');    
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');         
  next();
});

app.use(express.json());    // parser les données et les rendres en objet 

app.use("/api/employees", employeesRoutes);

module.exports = app;
