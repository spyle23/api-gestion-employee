const Employee = require("../model/employees"); //importation du schéma Employee
exports.employeesController = {
  getAllEmployees: async (req, res) => {
    try {
      const employees = await Employee.find();
      res.status(200).json({ employees });
    } catch (error) {
      res.status(400).json({ error });
    }
  },
  getEmployeeById: async (req, res) => {
    const idEmployee = req.params.id;
    try {
      const employee = await Employee.findById(idEmployee);
      res.status(200).json({ employee });
    } catch (error) {
      res.status(404).json({ error });
    }
  },
  createNewEmployee: async (req, res) => {
    try {
      const newEmployee = new Employee({ ...req.body });
      await newEmployee.save();
      res.status(201).json({ message: "employee enregistré" });
    } catch (error) {
      res.status(400).json({ error });
    }
  },
  deleteEmployeeById: async(req, res)=>{
    const idEmployee = req.params.id;
    try {
        await Employee.deleteOne({_id: idEmployee});
        res.status(200).json({message: "employee supprimé"});
    } catch (error) {
        res.status(400).json({error});
    }
  },
  updateEmployeeById: async(req, res)=>{
    const idEmployee = req.params.id;
    try {
        await Employee.updateOne({_id: idEmployee}, {...req.body});
        res.status(200).json({message: "employee modifiée"});
    } catch (error) {
        res.status(404).json({error});
    }
  }
};
