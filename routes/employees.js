const express = require('express');
const { employeesController } = require('../controller/employees');

const routes = express.Router();

routes.get('/', employeesController.getAllEmployees);
routes.get('/:id', employeesController.getEmployeeById);
routes.post('/', employeesController.createNewEmployee);
routes.delete('/:id', employeesController.deleteEmployeeById);
routes.put('/:id', employeesController.updateEmployeeById);


module.exports = routes;